# SDK 1.0.0

* 接口文档：[http://developer-doc.fapiaoer.cn/API%E5%BC%80%E7%A5%A8/%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D/%E4%BA%A7%E5%93%81%E8%AF%B4%E6%98%8E.html](http://developer-doc.fapiaoer.cn/API%E5%BC%80%E7%A5%A8/%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D/%E4%BA%A7%E5%93%81%E8%AF%B4%E6%98%8E.html)

## sdk版本

| 版本  | 地址                                        | commit号   | 本地文件夹     |
| ----- | ------------------------------------------- | ---------- | -------------- |
| 1.0.0 | git.feehi.com/lf/developer-platform-sdk.git | 1fab3ebc3e | /golang/sdk.go |
